﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaceObjectOnObject : MonoBehaviour
{
    public Transform ObjectToGo;

    // Start is called before the first frame update
    void Start()
    {
       

    }

    public void TransformToObject()
    {

        transform.position = ObjectToGo.position;
        transform.rotation = ObjectToGo.rotation;

    }

    
}
