﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using ScreenOrientation = MarksAssets.ScreenOrientationWebGL.ScreenOrientationWebGL.ScreenOrientation;

public class DeviceOrientation : MonoBehaviour
{

    public UnityEvent IsInLandscape;
    public UnityEvent NotInLandscape;
    public ScreenOrientation screenOrientation;

    public void setOrientation(int orient)
    {
        ScreenOrientation orientation = (ScreenOrientation)orient;

        if (orientation == ScreenOrientation.LandscapeLeft || orientation == ScreenOrientation.LandscapeRight)
        {
            IsInLandscape?.Invoke();

        }

        else if (orientation == ScreenOrientation.Portrait || orientation == ScreenOrientation.PortraitUpsideDown)
        {
            NotInLandscape?.Invoke();
        }
    }

}
