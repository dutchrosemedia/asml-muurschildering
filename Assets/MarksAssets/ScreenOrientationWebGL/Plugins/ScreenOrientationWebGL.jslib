mergeInto(LibraryManager.library, {
	ScreenOrientationWebGL_Start: function(orient) {
		if (Module.asmLibraryArg._ScreenOrientationWebGL_Start.updateOrient)
			return;
		Module.asmLibraryArg._ScreenOrientationWebGL_Start.orient = orient;
		Module.asmLibraryArg._ScreenOrientationWebGL_Start.orientBuf = Module.asmLibraryArg._ScreenOrientationWebGL_Start.orientBuf || new Int32Array(buffer, orient, 1);
		Module.asmLibraryArg._ScreenOrientationWebGL_Start.updateOrient = Module.asmLibraryArg._ScreenOrientationWebGL_Start.updateOrient || function () {
			if (Module.asmLibraryArg._ScreenOrientationWebGL_Start.orientBuf.byteLength === 0)//buffer changed size, need to get new reference
				Module.asmLibraryArg._ScreenOrientationWebGL_Start.orientBuf = new Int32Array(buffer, Module.asmLibraryArg._ScreenOrientationWebGL_Start.orient, 1);
			var ori = (screen.orientation || {}).type || screen.mozOrientation || screen.msOrientation;
			if (!ori)//safari
				ori = window.orientation === 0 ? "portrait-primary" : window.orientation === 180 ? "portrait-secondary" : window.orientation === 90 ? "landscape-primary" : "landscape-secondary";
			Module.asmLibraryArg._ScreenOrientationWebGL_Start.orientBuf[0] = ori === "portrait-primary" ? 0 : ori === "portrait-secondary" ? 1 : ori === "landscape-primary" ? 2 : 3;
			Module['ScreenOrientationWebGL'].onOrientationChange(Module.asmLibraryArg._ScreenOrientationWebGL_Start.orientBuf[0]);
		}
		Module.asmLibraryArg._ScreenOrientationWebGL_Start.updateOrient();
		window.addEventListener("orientationchange", Module.asmLibraryArg._ScreenOrientationWebGL_Start.updateOrient);
	},
	ScreenOrientationWebGL_Stop: function() {
		if (Module.asmLibraryArg._ScreenOrientationWebGL_Start.updateOrient) {
			window.removeEventListener("orientationchange", Module.asmLibraryArg._ScreenOrientationWebGL_Start.updateOrient);
			Module.asmLibraryArg._ScreenOrientationWebGL_Start.updateOrient = undefined;
		}
	}
});