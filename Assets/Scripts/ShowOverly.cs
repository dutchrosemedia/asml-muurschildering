using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowOverly : MonoBehaviour
{
    public bool SeeImage1;
    public bool SeeImage2;
    

    public GameObject Overlay;
    // Update is called once per frame

    private void Start()
    {
        CheckToShowImage();
    }


    public void IseeImage1(bool imageoneviewstatus)
    {
        SeeImage1 = imageoneviewstatus;

        CheckToShowImage();
    }



    public void IseeImage2(bool imagetoeviewstatus)
    {
        SeeImage2 = imagetoeviewstatus;

        CheckToShowImage();
    }

   


    public void CheckToShowImage()
    {

        if (SeeImage1 == false && SeeImage2 == false)
        {
            Overlay.SetActive(true);
        }
        else
        {
            Overlay.SetActive(false);

        }


    }


}
