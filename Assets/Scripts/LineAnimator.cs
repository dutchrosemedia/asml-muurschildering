using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineAnimator : MonoBehaviour
{
    [SerializeField] LineRenderer line;
    [SerializeField] Transform endPositionObject;
    Vector3 originalPos;
    Vector3 endPosition;
    [SerializeField] [Tooltip("The index of the point in the line to set the position of. Usually the last point.")] 
    int linePoint = 1;

    [Space]
    [SerializeField] bool disableUntilStart = true; // Whether to disable the lineRenderer until the line starts animating
    [SerializeField] float startDelay; // The time it takes for the lines to start moving
    [SerializeField] float lineSpeed; // The speed at which the line travels
    [SerializeField] bool loop = true; // Whether to loop the line or to end it once it reaches the endposition
    [SerializeField] bool smooth = false; // Whether to smoothly interpolate the line's movement
    [SerializeField] bool fade = true; // Whether to fade out the color of the line
    [SerializeField] [Tooltip("Whether to make the line thinner during the fade. Only works if fade is enabled.")] 
    bool thinLine = true; // Whether to thin the line during the fade

    [Space]
    [SerializeField] float distanceToEnd = .5f; // The distance from the endposition where the reset function will be called
    [SerializeField] float resetWaitTime = .5f; // The time to wait until the line is reset
    [SerializeField]
    [Tooltip("A value in seconds that is randomized from 0 to this number added to the resetTime so that multiple lines do not reset at the same time. Set higher for potential longer wait time. Set to 0 for none.")]
    float addedWaitTime = 1;

    [Space]
    [SerializeField] float fadeOutTime; // The time it takes for the line to fade out
    [SerializeField] float fadeInTime;

    float startSize;
    float endSize;
    float startAlpha;
    bool started;
    bool isResetting;
    bool isFading;
    bool hasBeenDisabled;

    // Start is called before the first frame update
    void Start()
    {
        InitialiseValue();

        // If the bool is true, disable the lineRenderer
        if (disableUntilStart)
        {
            line.enabled = false;
        }

        // Call a coroutine that waits for the delay time, then sets started to true
        StartCoroutine(StartDelay(startDelay));
    }

    private void OnEnable()
    {
        // Reset the line on re-enable
        if (hasBeenDisabled)
        {
            isFading = false;
            isResetting = false;
            StartCoroutine(ResetPosition(originalPos));
            StartCoroutine(StartDelay(startDelay));
        }
    }

    private void OnDisable()
    {
        // If the gameobject is disabled, stop coroutines and set the disabled bool to true
        hasBeenDisabled = true;
        started = false;

        if (disableUntilStart)
            line.enabled = false;

        StopAllCoroutines();
    }

    // Update is called once per frame
    void Update()
    {
        // Only run if started is true
        if (started)
        {
            // If the endposition changed, update it
            if (endPositionObject.localPosition != endPosition)
                endPosition = endPositionObject.localPosition;

            // Move the endpoint of the line to the end position
            if (smooth)
            {
                Debug.Log("Movin point");
                line.SetPosition(linePoint, Vector3.Lerp(line.GetPosition(linePoint), endPosition, lineSpeed * Time.deltaTime));
            }
            else
            {
                line.SetPosition(linePoint, Vector3.MoveTowards(line.GetPosition(linePoint), endPosition, lineSpeed * Time.deltaTime));
            }

            // If the endposition has almost been reached, call a function
            if (Vector3.Distance(line.GetPosition(linePoint), endPosition) < distanceToEnd && !isResetting)
            {
                // Only reset if loop is enabled
                if (loop)
                {
                    isResetting = true;
                    StartCoroutine(ResetPosition(originalPos));
                }
            }
        }
    }

    void InitialiseValue()
    {
        originalPos = line.GetPosition(1);
        endPosition = endPositionObject.localPosition;

        startAlpha = line.startColor.a;
        startSize = line.startWidth;
        endSize = line.endWidth;
    }

    // Resets the train's position to the startPosition
    IEnumerator ResetPosition(Vector3 pos)
    {
        // Wait for the line to be faded out
        if (fade && !isFading)
        {
            isFading = true;
            yield return StartCoroutine(FadeLine(false, fadeOutTime));
        }

        // Wait for wait time
        float waitTime = resetWaitTime + Random.Range(0, addedWaitTime);
        yield return new WaitForSeconds(waitTime);

        // Then reset the position
        line.SetPosition(linePoint, pos);

        // Fade back in
        if (fade && !isFading)
        {
            isFading = true;
            yield return StartCoroutine(FadeLine(true, fadeInTime));
        }

        // Set isResetting to false so it can restart the resetting process
        isResetting = false;
    }

    // Fades the line in or out
    IEnumerator FadeLine(bool fadeIn, float time)
    {
        float currentTime = 0;

        Color startColor = line.startColor;
        Color endColor = line.endColor; 
        
        // If fading in and thinline is true, reset the width
        if (thinLine && fadeIn)
        {
            line.startWidth = startSize;
            line.endWidth = endSize;
        }

        while (currentTime < time)
        {
            currentTime += Time.deltaTime;

            // If fadeIn is true, fade it in, if false, fade out
            if (fadeIn)
            {
                startColor.a = Mathf.Lerp(0, startAlpha, currentTime / time);
                endColor.a = Mathf.Lerp(0, startAlpha, currentTime / time);

                line.startColor = startColor;
                line.endColor = endColor;
            }
            else
            {
                startColor.a = Mathf.Lerp(startAlpha, 0, currentTime / time);
                endColor.a = Mathf.Lerp(startAlpha, 0, currentTime / time);

                line.startColor = startColor;
                line.endColor = endColor;
            }

            // If thinLine is true, make the line thinner over time
            if (thinLine && !fadeIn)
            {
                line.startWidth = Mathf.Lerp(startSize, 0, currentTime / time);
                line.endWidth = Mathf.Lerp(endSize, 0, currentTime / time);
            }

            yield return null;
        }

        isFading = false;
        yield break;
    }

    IEnumerator StartDelay(float delay)
    {
        yield return new WaitForSeconds(delay);

        if (disableUntilStart)
        {
            line.enabled = true;
        }

        started = true;
    }
}
